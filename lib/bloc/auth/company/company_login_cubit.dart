import 'package:bloc/bloc.dart';
import 'package:informermobile_flutter/bloc/ResultState.dart';
import 'package:informermobile_flutter/data/api_service/base/ApiResult.dart';
import 'package:informermobile_flutter/data/api_service/base/NetworkExceptions.dart';
import 'package:informermobile_flutter/data/repository/AuthRepository.dart';

class CompanyLoginCubit extends Cubit<ResultState<bool>> {
  final AuthRepository _apiRepository;

  CompanyLoginCubit(this._apiRepository)
      : assert(_apiRepository != null),
        super(Idle());

  companyLogin(Map<String, dynamic> queryParameters) async {
    emit(ResultState.loading());
    ApiResult<bool> successLogin =
        await _apiRepository.companyLogin(queryParameters);
    successLogin.when(success: (bool data) {
      emit(ResultState.data(data: data));
    }, failure: (NetworkExceptions error) {
      emit(ResultState.error(error: error));
    });
  }
}