import 'package:informermobile_flutter/data/api_service/base/ApiResult.dart';
import 'package:informermobile_flutter/data/repository/AuthRepository.dart';

class AuthViewModel {
  AuthRepository _authRepository = AuthRepository();

  Future<bool> isUserLogged() async {
    bool _isUserLogged;
    await _authRepository.isLoggedIn.then((value) => _isUserLogged = value);
    return _isUserLogged;
  }


}
