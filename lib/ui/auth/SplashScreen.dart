import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:informermobile_flutter/ui/auth/AuthViewModel.dart';
import 'package:informermobile_flutter/ui/auth/CompanyLoginScreen.dart';
import 'package:informermobile_flutter/ui/home/HomeScreen.dart';
import 'package:informermobile_flutter/utils/data/NavigationUtility.dart';
import 'package:informermobile_flutter/utils/style/AppColors.dart';
import 'package:informermobile_flutter/utils/views/ImagesUtility.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AuthViewModel _authViewModel;

  @override
  void initState() {
    super.initState();
    _authViewModel = AuthViewModel();
    _handleUserSession();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: _body());
  }

  Widget _body() {
    return Container(
      child: _backgroundImage(),
    );
  }

  Widget _backgroundImage() {
    return Container(
      constraints: BoxConstraints.expand(),
      decoration: BoxDecoration(
        image: DecorationImage(
            colorFilter: ColorFilter.mode(
                blackColor.withOpacity(0.5), BlendMode.srcOver),
            image: assetJpgImage(context: context, imageName: "splash_bg"),
            fit: BoxFit.cover),
      ),
      child: _centerLogo(),
    );
  }

  Widget _centerLogo() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        assetPngImage(
            context: context,
            imageName: "logo_white",
            height: 100,
            width: 100,
            color: whiteColor),
      ],
    );
  }

  _handleUserSession() async {
    await Future.delayed(Duration(seconds: 3)); // a simulated delay
    _authViewModel.isUserLogged().then((isUserLogged) {
      if (isUserLogged) {
        print("handleUserSession => session valid");
        _autoLogin();
      } else {
        print("handleUserSession => session expired");
        navigateAndClearStack(
            context: context, destinationScreen: CompanyLoginScreen());
      }
    }).catchError((error) {
      print("Error handleUserSession => " + error.toString());
      navigateAndClearStack(
          context: context, destinationScreen: CompanyLoginScreen());
    });
  }

  _autoLogin() {
    // do login logic then navigate to home screen
    navigateAndClearStack(context: context, destinationScreen: HomeScreen());
  }
}
