import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:informermobile_flutter/utils/data/ValidationUtility.dart';
import 'package:informermobile_flutter/utils/style/SpacingUtility.dart';
import 'package:informermobile_flutter/utils/style/AppColors.dart';
import 'package:informermobile_flutter/utils/style/AppTextStyles.dart';
import 'package:informermobile_flutter/utils/views/ButtonsUtility.dart';
import 'package:informermobile_flutter/utils/views/SizedBoxUtility.dart';
import 'package:informermobile_flutter/utils/views/TextInputUtility.dart';

class UserLoginScreen extends StatefulWidget {
  @override
  _UserLoginScreenState createState() => _UserLoginScreenState();
}

class _UserLoginScreenState extends State<UserLoginScreen> {
  // vars
  final _formKey = GlobalKey<FormState>();
  String _emailValue;
  String _passwordValue;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
    );
  }

  Widget _body() {
    return Container(
      padding: paddingAll(context: context, size: 32),
      child: Center(
        child: _companyLoginForm(),
      ),
    );
  }

  Widget _companyLoginForm() {
    return Form(
      key: _formKey,
      child: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'User Login',
              style: primaryTextStyle(context),
            ),
            verticalSizedBox(context: context, size: 25),
            buildCrudTextFormField(
                context: context,
                icon: null,
                hintText: "Email",
                inputType: TextInputType.emailAddress,
                isObscureText: false,
                onSaved: (input) => _emailValue = input.trim(),
                validator: (input) => validateEmptyInput(input)
                    ? validateEmail(input)
                        ? null
                        : invalidEmail
                    : requiredField),
            verticalSizedBox(context: context, size: 10),
            buildCrudTextFormField(
                context: context,
                icon: null,
                hintText: "Password",
                inputType: TextInputType.visiblePassword,
                isObscureText: true,
                onSaved: (input) => _passwordValue = input.trim(),
                validator: (input) => validateEmptyInput(input)
                    ? validatePassword(input)
                        ? null
                        : invalidPassword
                    : requiredField),
            verticalSizedBox(context: context, size: 25),
            buildPrimaryButton(
                context: context,
                // height: 50,
                width: 220,
                color: appPrimaryColor,
                text: 'Login',
                callback: () {
                  _submit();
                }),
          ],
        ),
      ),
    );
  }

  _submit() {
    FocusScope.of(context).unfocus();
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
    }
  }
}
