import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:informermobile_flutter/ui/auth/UserLoginScreen.dart';
import 'package:informermobile_flutter/utils/data/NavigationUtility.dart';
import 'package:informermobile_flutter/utils/data/ValidationUtility.dart';
import 'package:informermobile_flutter/utils/style/SpacingUtility.dart';
import 'package:informermobile_flutter/utils/style/AppColors.dart';
import 'package:informermobile_flutter/utils/style/AppTextStyles.dart';
import 'package:informermobile_flutter/utils/views/ButtonsUtility.dart';
import 'package:informermobile_flutter/utils/views/SizedBoxUtility.dart';
import 'package:informermobile_flutter/utils/views/TextInputUtility.dart';

class CompanyLoginScreen extends StatefulWidget {
  @override
  _CompanyLoginScreenState createState() => _CompanyLoginScreenState();
}

class _CompanyLoginScreenState extends State<CompanyLoginScreen> {
  // vars
  final _formKey = GlobalKey<FormState>();
  String _companyNameValue;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _body(),
    );
  }

  Widget _body() {
    return Container(
      padding: paddingAll(context: context, size: 32),
      child: Center(
        child: _companyLoginForm(),
      ),
    );
  }

  Widget _companyLoginForm() {
    return Form(
      key: _formKey,
      child: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'Company Login',
              style: primaryTextStyle(context),
            ),
            verticalSizedBox(context: context, size: 25),
            buildCrudTextFormField(
                context: context,
                icon: null,
                hintText: "Company name",
                inputType: TextInputType.visiblePassword,
                isObscureText: false,
                onSaved: (input) => _companyNameValue = input.trim(),
                validator: (input) => validateEmptyInput(input)
                    ? validateName(input)
                        ? null
                        : invalidCompanyName
                    : requiredField),
            verticalSizedBox(context: context, size: 25),
            buildPrimaryButton(
                context: context,
                // height: 50,
                width: 220,
                color: appPrimaryColor,
                text: 'Start',
                callback: () {
                  _submit();
                }),
          ],
        ),
      ),
    );
  }

  _submit() {
    FocusScope.of(context).unfocus();
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      navigate(context: context, destinationScreen: UserLoginScreen());
    }
  }
}
