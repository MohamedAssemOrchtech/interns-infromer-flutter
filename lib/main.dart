import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:informermobile_flutter/ui/auth/SplashScreen.dart';
import 'package:informermobile_flutter/utils/style/AppTextStyles.dart';
import 'package:informermobile_flutter/utils/style/MyAppThemeData.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Informer',
      debugShowCheckedModeBanner: false,
      theme: MyAppThemeData().appThemeData(context),
      localizationsDelegates: [
        // Built-in localization of basic text for Material widgets
        GlobalMaterialLocalizations.delegate,
        // Built-in localization for text direction LTR/RTL
        GlobalWidgetsLocalizations.delegate,
        // Built-in localization of basic text for Cupertino widgets
        GlobalCupertinoLocalizations.delegate,
      ],
      builder: (BuildContext context, Widget widget) {
        ErrorWidget.builder = (FlutterErrorDetails errorDetails) {
          return buildErrorWidger(context, errorDetails);
        };
        return widget;
      },
      home: SplashScreen(),
    );
  }
}

Widget buildErrorWidger(BuildContext context, FlutterErrorDetails error) {
  return Expanded(
    child: Center(
      child: Text(
        "Error happened",
        style: errorTextStyle(context),
      ),
    ),
  );
}
