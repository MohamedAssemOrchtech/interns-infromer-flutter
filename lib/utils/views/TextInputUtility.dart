import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:informermobile_flutter/utils/style/AppColors.dart';
import 'package:informermobile_flutter/utils/style/AppTextStyles.dart';
import 'package:informermobile_flutter/utils/views/MyAppBorders.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

Widget buildFormLabel({BuildContext context, String text, bool isRequired}) {
  return Container(
    padding: const EdgeInsets.fromLTRB(4, 0, 4, 4),
    child: Row(
      children: [
        if (isRequired)
          Row(
            children: [
              Text("*", style: requiredIconTextStyle(context)),
              SizedBox(
                width: ResponsiveFlutter.of(context).scale(8),
              ),
            ],
          ),
        Expanded(
          child: Text(
            text,
            style: secondaryTextStyle(context),
          ),
        )
      ],
    ),
  );
}

Widget buildCrudTextFormField({
  BuildContext context,
  bool isObscureText = false,
  Icon icon,
  String initialValue,
  String hintText,
  bool readOnly,
  TextEditingController controller,
  GestureTapCallback onTap,
  TextInputType inputType,
  void onSaved(String input),
  void onChanged(String input),
  String validator(String input),
}) {
  return Container(
    child: TextFormField(
      initialValue: initialValue,
      style: primaryTextStyle(context),
      validator: validator,
      keyboardType: inputType,
      textInputAction: TextInputAction.done,
      onSaved: onSaved,
      onChanged: onChanged,
      controller: controller,
      autofocus: false,
      obscureText: isObscureText,
      onTap: onTap,
      readOnly: readOnly != null ? readOnly : false,
      decoration: new InputDecoration(
        contentPadding: EdgeInsets.symmetric(
            horizontal: ResponsiveFlutter.of(context).scale(25),
            vertical: ResponsiveFlutter.of(context).verticalScale(10)),
        fillColor: whiteColor,
        prefixIcon: icon,
        enabledBorder: formInputBorderStyle(context),
        errorBorder: formInputBorderErrorStyle(context),
        focusedBorder: focusedFormInputBorderStyle(context),
        focusedErrorBorder: focusedFormInputBorderStyle(context),
        errorStyle: errorTextStyle(context),
        filled: true,
        hintStyle: hintTextStyle(context),
        hintText: hintText,
      ),
    ),
  );
}

/*
inputFormatters: [new WhitelistingTextInputFormatter(RegExp("[0-9]"))],

TextField(
          maxLength: 3,
          inputFormatters: <TextInputFormatter>[
            WhitelistingTextInputFormatter.digitsOnly,
          ],
          keyboardType: TextInputType.number,
        ),
 */
/*

        isPasswordVisible = false;

        child: TextFormField(
                  controller: _textEditConConfirmPassword,
                  focusNode: _passwordConfirmFocus,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.done,
                  validator: _validateConfirmPassword,
                  obscureText: !isConfirmPasswordVisible,
                  decoration: InputDecoration(
                      labelText: 'Confirm Password',
                      suffixIcon: IconButton(
                        icon: Icon(isConfirmPasswordVisible
                            ? Icons.visibility
                            : Icons.visibility_off),
                        onPressed: () {
                          setState(() {
                            isConfirmPasswordVisible =
                                !isConfirmPasswordVisible;
                          });
                        },
                      ),
                      icon: Icon(Icons.vpn_key))),
            ),
 */
