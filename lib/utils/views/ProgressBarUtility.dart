import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:informermobile_flutter/utils/style/AppColors.dart';
import 'package:informermobile_flutter/utils/style/AppTextStyles.dart';
import 'package:loading_indicator_view/loading_indicator_view.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

final Widget appProgressBar = BallClipRotatePulseIndicator(
  color: appPrimaryColor,
  solidCircleRadius: 8,
  minRadius: 8,
);

CircularPercentIndicator circularPercentIndicator({
  BuildContext context,
  double radius,
  double lineWidth,
  double percentage,
  String text,
  Color color,
}) {
  return CircularPercentIndicator(
    radius: radius,
    lineWidth: lineWidth,
    percent: percentage,
    center: Container(
      padding: EdgeInsets.all(ResponsiveFlutter.of(context).moderateScale(10)),
      child: new Text(
        text,
        style: secondaryTextStyle(context),
      ),
    ),
    progressColor: color,
  );
}
