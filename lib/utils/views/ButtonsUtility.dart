import 'package:flutter/material.dart';
import 'package:informermobile_flutter/utils/style/AppTextStyles.dart';
import 'package:informermobile_flutter/utils/views/MyAppBorders.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

MaterialButton buildPrimaryButton(
    {BuildContext context,
    double width,
    Color color,
    String text,
    VoidCallback callback}) {
  return MaterialButton(
    // height: ResponsiveFlutter.of(context).scale(12.5),
    minWidth: ResponsiveFlutter.of(context).scale(width),
    padding: EdgeInsets.all(ResponsiveFlutter.of(context).moderateScale(12.5)),
    color: color,
    shape: buttonShape(context: context, color: color),
    onPressed: callback,
    child: Text(
      text,
      style: buttonTextStyle(context),
      // style: buttonTextStyle(context, textSize, textColor),
    ),
  );
}