import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:informermobile_flutter/utils/style/AppColors.dart';

class MyAppThemeData {
  static const MaterialColor appThemeColor = MaterialColor(
    0xff39AD6E,
    <int, Color>{
      50: Color(0xff285780),
      100: Color(0xff285780),
      200: Color(0xff285780),
      300: Color(0xff285780),
      400: Color(0xff285780),
      500: Color(0xff285780),
      600: Color(0xff285780),
      700: Color(0xff285780),
      800: Color(0xff285780),
      900: Color(0xff285780),
    },
  );

  ThemeData appThemeData(BuildContext context) {
    return ThemeData(
        primaryColor: appPrimaryColor,
        textTheme: GoogleFonts.ubuntuTextTheme(Theme.of(context).textTheme),
        pageTransitionsTheme: PageTransitionsTheme(builders: {
          TargetPlatform.iOS: CupertinoPageTransitionsBuilder(),
          TargetPlatform.android: CupertinoPageTransitionsBuilder(),
        }),
        scaffoldBackgroundColor: backgroundColor,
        primarySwatch: MyAppThemeData.appThemeColor,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        appBarTheme: AppBarTheme(
          brightness: Brightness.dark,
        ));
  }
}
