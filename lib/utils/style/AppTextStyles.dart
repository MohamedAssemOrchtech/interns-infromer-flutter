// text sizes
import 'dart:ui';

import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:informermobile_flutter/utils/style/AppColors.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

final double primaryTextSize = 2.3;
final double secondaryTextSize = 2.0;
final double toastMessageTextSize = 2.0;
final double hintMessageTextSize = 2.0;
final double errorMessageTextSize = 2.0;

// font weights
final FontWeight primaryTextFontWeight = FontWeight.w500;
final FontWeight secondaryTextFontWeight = FontWeight.w400;
final FontWeight hintTextFontWeight = FontWeight.w300;
final FontWeight errorTextFontWeight = FontWeight.w300;

TextStyle primaryTextStyle(BuildContext context) {
  return GoogleFonts.ubuntu(
      fontWeight: primaryTextFontWeight,
      fontSize: ResponsiveFlutter.of(context).fontSize(primaryTextSize),
      color: primaryTextColor);
}

TextStyle secondaryTextStyle(BuildContext context) {
  return GoogleFonts.ubuntu(
      fontWeight: secondaryTextFontWeight,
      fontSize: ResponsiveFlutter.of(context).fontSize(secondaryTextSize),
      color: secondaryTextColor);
}

TextStyle requiredIconTextStyle(BuildContext context) {
  return GoogleFonts.ubuntu(
      fontWeight: secondaryTextFontWeight,
      fontSize: ResponsiveFlutter.of(context).fontSize(secondaryTextSize),
      color: alertColor);
}

TextStyle hintTextStyle(BuildContext context) {
  return GoogleFonts.ubuntu(
      fontWeight: hintTextFontWeight,
      fontSize: ResponsiveFlutter.of(context).fontSize(hintMessageTextSize),
      color: hintCrudColor);
}

TextStyle errorTextStyle(BuildContext context) {
  return GoogleFonts.ubuntu(
      fontWeight: errorTextFontWeight,
      fontSize: ResponsiveFlutter.of(context).fontSize(errorMessageTextSize),
      color: alertColor);
}

// buttons
TextStyle buttonTextStyle(BuildContext context) {
  return GoogleFonts.tajawal(
      fontWeight: primaryTextFontWeight,
      fontSize: ResponsiveFlutter.of(context).fontSize(primaryTextSize),
      color: whiteColor);
}
