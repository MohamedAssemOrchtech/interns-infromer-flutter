import 'package:flutter/material.dart';


// theme colors
final Color appPrimaryColor = Color(0xff285780);
final Color appSecondaryColor = Color(0xff4581C3);

// text color
final Color primaryTextColor = Color(0xff1D1D35);
final Color secondaryTextColor = Color(0xffE6E7E8);

// main colors
final Color backgroundColor = Color(0xffFAFAFA);
final Color backgroundLowColor = Color(0xccf4f4f4);
final Color whiteColor = Color(0xffFFFFFF);
final Color blackColor = Color(0xff000000);
final Color activeIconColor = Color(0xff73C599);
final Color inActiveIconColor = Color(0xff757583);
final Color inActiveIconLowColor = Color(0x1a757583);
final Color hintCrudColor = Color(0xff757583);
final Color transparentColor = Color(0x00ffffff);
final Color alertColor = Color(0xffEB5050);
final Color formBorderColor = Color(0xffEFEFEF);

// Toasts Colors
final Color toastSuccessColor = Color(0xff44CCB1);
final Color toastInfoColor = Color(0xff52B0F8);
final Color toastAlertColor = Color(0xffFFC241);
final Color toastErrorColor = Color(0xffF66F84);
