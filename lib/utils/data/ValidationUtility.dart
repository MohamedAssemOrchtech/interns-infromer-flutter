// validation message
bool validateEmptyInput(String input) {
  return input.replaceAll(new RegExp(r"\s+"), "").length > 0;
}

bool validateName(String input) {
  if (validateEmptyInput(input)) {
    Pattern pattern = '^[a-z A-Z,.\-]+\$';
    RegExp regex = RegExp(pattern);
    if (regex.hasMatch(input)) return true;
  }
  return false;
}

bool validateNumber(String input) {
  if (validateEmptyInput(input)) {
    Pattern pattern = '^[1-9]+[0-9]*\$';
    RegExp regex = RegExp(pattern);
    if (regex.hasMatch(input)) return true;
  }
  return false;
}

bool validateNumberOrEmpty(String input) {
  if (validateEmptyInput(input)) {
    Pattern pattern = '^[1-9]+[0-9]*\$';
    RegExp regex = RegExp(pattern);
    if (input.isEmpty || regex.hasMatch(input)) return true;
  }
  return false;
}

bool validatePhoneNumber(String input) {
  if (validateEmptyInput(input)) {
    Pattern pattern =
        '^(\\+201[0125]\\d{8}|\\+9665[503649187]\\d{7}|\\+965[569]\\d{7}|\\+971(2|3|4|6|7|9|50|52|54|55|56|58)\\d{7})\$';
    RegExp regex = RegExp(pattern);
    if (regex.hasMatch(input)) return true;
  }
  return false;
}

bool validatePassword(String input) {
  if (validateEmptyInput(input)) {
    Pattern pattern = '^([a-zA-Z0-9@*#]{6,})\$';
    RegExp regex = RegExp(pattern);
    if (regex.hasMatch(input)) return true;
  }
  return false;
}

bool validateDropDown({String initialValue, String input}) {
  if (initialValue != input) return true;
  return false;
}

bool validateEmail(String input) {
  return RegExp(
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
      .hasMatch(input);
}

// extension validateEmail on String {
//   bool isValidEmail() {
//     return RegExp(
//         r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
//         .hasMatch(this);
//   }
// }

String requiredField = "This filed is required";
String invalidCompanyName = "Invalid company name";
String invalidEmail = "Invalid email";
String invalidPassword = "Invalid password";
