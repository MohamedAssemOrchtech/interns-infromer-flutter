import 'package:intl/intl.dart';

class DateUtility {
  String convertFromIsoToString(String date) {
    String formattedDate;
    try {
      DateTime tempDate = new DateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(date);
      formattedDate = DateFormat("d/M/yyyy").format(tempDate);
    } on Exception catch (exception) {
      // only executed if error is of type Exception
      formattedDate = exception.toString();
    } catch (error) {
      // executed for errors of all types other than Exception
      formattedDate = error.toString();
    }
    return formattedDate;
  }

  String getDayFromDate(String date) {
    String formattedDate;
    try {
      DateTime tempDate = new DateFormat("yyyy-MM-dd'T'HH:mm:ssZ").parse(date);
      formattedDate = DateFormat("d").format(tempDate);
    } on Exception catch (exception) {
      // only executed if error is of type Exception
      formattedDate = exception.toString();
    } catch (error) {
      // executed for errors of all types other than Exception
      formattedDate = error.toString();
    }
    return formattedDate;
  }

  String getCurrentMonth() {
    String formattedDate;
    try {
      DateTime tempDate = new DateTime.now();
      formattedDate = DateFormat("MM").format(tempDate);
    } on Exception catch (exception) {
      // only executed if error is of type Exception
      formattedDate = exception.toString();
    } catch (error) {
      // executed for errors of all types other than Exception
      formattedDate = error.toString();
    }
    return formattedDate;
  }

  String convertFromDatePickerToView(String date) {
    // 2020-12-23 00:00:00.000
    String formattedDate;
    try {
      DateTime tempDate = new DateFormat("yyyy-MM-dd HH:mm:ssZ").parse(date);
      formattedDate = DateFormat("d/M/yyyy").format(tempDate);
    } on Exception catch (exception) {
      // only executed if error is of type Exception
      formattedDate = exception.toString();
    } catch (error) {
      // executed for errors of all types other than Exception
      formattedDate = error.toString();
    }
    return formattedDate;
  }

  String convertFromDatePickerToBackend(String date) {
    // 2020-12-23 00:00:00.000
    String formattedDate;
    try {
      DateTime tempDate = new DateFormat("yyyy-MM-dd HH:mm:ssZ").parse(date);
      formattedDate = DateFormat("yyyy-MM-dd").format(tempDate);
    } on Exception catch (exception) {
      // only executed if error is of type Exception
      formattedDate = exception.toString();
    } catch (error) {
      // executed for errors of all types other than Exception
      formattedDate = error.toString();
    }
    return formattedDate;
  }
}
