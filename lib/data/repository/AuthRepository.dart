import 'package:informermobile_flutter/data/api_service/base/ApiResult.dart';
import 'package:informermobile_flutter/data/api_service/base/NetworkExceptions.dart';
import 'package:informermobile_flutter/data/api_service/clients/AuthClient.dart';
import 'package:informermobile_flutter/data/model/auth/UserLoginResponse.dart';
import 'package:informermobile_flutter/data/model/auth/CompanyLoginResponse.dart';
import 'package:informermobile_flutter/data/shared_pref/SharedPreferenceHelper.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthRepository {
  AuthClient _authClient = AuthClient();

  // user session
  Future<bool> get isLoggedIn async {
    return await SharedPreferenceHelper(SharedPreferences.getInstance())
        .isLoggedIn;
  }

  Future<User> get currentLoggedUser async {
    return await SharedPreferenceHelper(SharedPreferences.getInstance())
        .currentUser;
  }

  Future<void> logout() async {
    await SharedPreferenceHelper(SharedPreferences.getInstance())
        .removeCurrentUser();
  }

  // login
  Future<ApiResult<bool>> companyLogin(
      Map<String, dynamic> queryParameters) async {
    try {
      final response = await _authClient.companyLogin(queryParameters);
      CompanyLoginResponse companyLoginResponse =
          CompanyLoginResponse.fromJson(response);
      return ApiResult.success(data: companyLoginResponse.data.found);
    } catch (e) {
      return ApiResult.failure(error: NetworkExceptions.getDioException(e));
    }
  }
}
