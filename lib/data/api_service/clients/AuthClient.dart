import 'package:dio/dio.dart';
import 'package:informermobile_flutter/data/api_service/base/ApiClient.dart';
import 'package:informermobile_flutter/data/api_service/constants/EndPoints.dart';

class AuthClient {
  ApiClient _apiClient = ApiClient.getInstance;

  Future<Response> companyLogin(Map<String, dynamic> queryParameters) {
    return _apiClient.get(Endpoints.companyLoginUrl,
        queryParameters: queryParameters);
  }
}
