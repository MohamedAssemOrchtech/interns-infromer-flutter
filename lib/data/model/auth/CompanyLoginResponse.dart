/// data : {"found":true,"subdomain":"rana","errors":null}

class CompanyLoginResponse {
  Data _data;

  Data get data => _data;

  CompanyLoginResponse({
      Data data}){
    _data = data;
}

  CompanyLoginResponse.fromJson(dynamic json) {
    _data = json["data"] != null ? Data.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    return map;
  }

}

/// found : true
/// subdomain : "rana"
/// errors : null

class Data {
  bool _found;
  String _subdomain;
  dynamic _errors;

  bool get found => _found;
  String get subdomain => _subdomain;
  dynamic get errors => _errors;

  Data({
      bool found, 
      String subdomain, 
      dynamic errors}){
    _found = found;
    _subdomain = subdomain;
    _errors = errors;
}

  Data.fromJson(dynamic json) {
    _found = json["found"];
    _subdomain = json["subdomain"];
    _errors = json["errors"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["found"] = _found;
    map["subdomain"] = _subdomain;
    map["errors"] = _errors;
    return map;
  }

}